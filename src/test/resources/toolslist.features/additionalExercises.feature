Feature: Additional exercises
@additional
  Scenario: Able to enter text into Javascript input and retrieve text.
    Given I navigate to the AdditionalExercise page
    When I add text via the Say Something button and confirm
    Then the text is displayed in the section below
