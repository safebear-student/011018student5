package com.safebear.auto.tests;

import com.safebear.auto.pages.LoginPage;
import com.safebear.auto.pages.ToolsPage;
import com.safebear.auto.utils.Utils;
import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class StepDefs {
    WebDriver driver;
    LoginPage loginPage;
    ToolsPage toolsPage;

    @Before
    public void setUp() {
        driver = Utils.getDriver();
        loginPage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);


    }

    @After
    public void tearDown() {
        try {
            Thread.sleep(Integer.parseInt(System.getProperty("sleep", "2000")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.quit();
    }

    @Given("^I navigate to the login page$")
    public void i_navigate_to_the_login_page() throws Throwable {
        // Step 1 ACTION: Open our web application in the browser
        driver.get(Utils.getUrl());
        Assert.assertEquals("Login Page", loginPage.getPageTitle(), "We're not on the login page or its title has changed");
    }

    @When("^I enter the login details for a '(.+)'$")
    public void i_enter_the_login_details_for_a_USER(String user) throws Throwable {
        switch (user) {
            case "invalidUser":
                loginPage.enterUsername("attacker");
                loginPage.enterPassword("dontletmein");
                loginPage.clickLoginButton();
                break;

            case "validUser":
                loginPage.enterUsername("tester");
                loginPage.enterPassword("letmein");
                loginPage.clickLoginButton();
                break;

            default:
                Assert.fail("the test data is wrong- the only values that can be accepted are 'validUser' or 'invalidUser'");
                break;
        }
    }

    @Then("^I can see the following message: '(.+)'$")
    public void i_can_see_the_following_MESSAGE(String message) throws Throwable {
       switch (message) {
           case "Username or Password is incorrect":
               Assert.assertTrue(loginPage.checkForFailedLoginWarning().contains(message));
               break;
           case "Login Successful" :
               Assert.assertTrue(toolsPage.checkForLoginSuccessfulMessage().contains(message));
               break;
           default:
               Assert.fail("the test data is wrong");
               break;
       }
    }

    @Given("^I navigate to the Tools Page$")
    public void iNavigateToTheToolsPage() throws Throwable {
        driver.get(Utils.getUrl());
        loginPage.enterUsername("tester");
        loginPage.enterPassword("letmein");
        loginPage.clickLoginButton();
        Assert.assertEquals(toolsPage.getPageTitle(),"Tools Page","wrong page- expected tools page");
    }

    @When("^I search for an existing tool on the table via the search field$")
    public void iSearchForAnExistingToolOnTheTableViaTheSearchField() throws Throwable {

    }

    //Search Feature
    @Given("^that I am logged in$")
    public void thatIAmLoggedIn() throws Throwable {
        driver.get(Utils.getUrl());
        loginPage.enterUsername("tester");
        loginPage.enterPassword("letmein");
        loginPage.clickLoginButton();
        Assert.assertEquals(toolsPage.getPageTitle(),"Tools Page","wrong page- expected tools page");
          }

    @Given("^the (.+) tool exists$")
    public void theToolExists(String toolName) throws Throwable {
       // Assert.assertEquals(toolsPage.checkToolExists(toolName),"tool does not exist");

    }

    @When("^I search for the (.+) tool$")
    public void iSearchForTheTool(String toolName) throws Throwable {
        toolsPage.enterTextIntoSearchField(toolName);
        toolsPage.clickOnSearchButton();

    }

    @Then("^the (.+) tool is returned$")
    public void theToolIsReturned(String toolName) throws Throwable {
    //    Assert.assertEquals(toolName,toolsPage.checkToolExists(),"tool does not exist");

    }

//additional exercises
    @Given("^I navigate to the AdditionalExercise page$")
    public void iNavigateToTheAdditionalExercisePage() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @When("^I add text via the Say Something button and confirm$")
    public void iAddTextViaTheSaySomethingButtonAndConfirm() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^the text is displayed in the section below$")
    public void theTextIsDisplayedInTheSectionBelow() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }
}
