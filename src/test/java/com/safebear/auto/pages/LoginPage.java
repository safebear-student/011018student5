package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.LoginPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class LoginPage {

    LoginPageLocators locators = new LoginPageLocators();
    //we want lombok to ensure webdriver is retrieving driver and not creating, hence nonnull
    @NonNull
    WebDriver driver;

    //get title of webpage - tostring no longer required
    public String getPageTitle() {
        return driver.getTitle().toString();
    }

    // username sent to field
    public void enterUsername(String username) {
        driver.findElement(locators.getUsernameLocator()).sendKeys(username);

    }

    // password sent to field
    public void enterPassword(String password) {
        driver.findElement(locators.getPasswordLocator()).sendKeys(password);

    }

    // login button clicked
    public void clickLoginButton() {
        driver.findElement(locators.getLoginButtonLocator()).click();

    }

    //when failed login, grab warning text
    public String checkForFailedLoginWarning() {
        return driver.findElement(locators.getFailedLoginMessage()).getText();
    }

}
