package com.safebear.auto.pages;

import com.safebear.auto.pages.locators.ToolsPageLocators;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class ToolsPage {
    ToolsPageLocators toolsPageLocators = new ToolsPageLocators();

    @NonNull
    WebDriver driver;

    public String getPageTitle() {
        return driver.getTitle().toString();
    }

    public String checkForLoginSuccessfulMessage() {
        return driver.findElement(toolsPageLocators.getSuccessfulLoginMessage()).getText();
    }

    //Entering text into the search field
    public void enterTextIntoSearchField(String searchentrytext) {
        driver.findElement(toolsPageLocators.getSearchField()).sendKeys(searchentrytext);
    }

    // clicking on the search button
    public void clickOnSearchButton() {
        driver.findElement(toolsPageLocators.getSearchButton()).click();
    }

    // checking that the selenium tool has been returned /checking it exists
    public boolean checkToolExists(String toolName) {
        return driver.findElement(toolsPageLocators.getSeleniumTool()).isDisplayed();
          }
    }

