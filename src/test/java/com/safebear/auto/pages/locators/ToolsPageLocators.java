package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;

@Data
public class ToolsPageLocators {

    //message checks
    private By successfulLoginMessage = By.xpath(".//*[@class='container']/p/b");
    //extra places - search here field
    private By searchField = By.xpath(".//input[contains(@placeholder,'Type the Name')]");
    //buttons
    private By searchButton = By.xpath(".//button[@class='btn btn-info']");
    //selenium tool from the table
    private By seleniumTool = By.xpath("//td[.='Selenium']");
}
